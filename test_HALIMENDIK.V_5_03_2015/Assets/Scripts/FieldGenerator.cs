﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(FieldConfig))]
public class FieldGenerator : MonoBehaviour
{
    public Texture2D background;
    public Texture2D apple;
    public Texture2D grape;
    public Texture2D pear;
    public Texture2D mandarine;
    private Texture2D[,] fruitsMatrix;

    // Use this for initialization
    void Start()
    {
        fruitsMatrix = CreateFruitMatrix();
    }

    /// <summary>
    /// Создание игрового поля, создаем фон, создаем сетку из фишек.
    /// </summary>

    void OnGUI()
    {
        GUI.Label(new Rect(FieldConfig.FIELD_OFFSET_X, FieldConfig.FIELD_OFFSET_Y, FieldConfig.FIELD_WIDHT, FieldConfig.FIELD_HEIGHT), background);
        float y = FieldConfig.FRUIT_Y;
        for (int collumn = 0; collumn < fruitsMatrix.GetLength(0); collumn++, y += FieldConfig.OFFSET_Y)
        {
            float x = FieldConfig.FRUIT_Y;
            for (int line = 0; line < fruitsMatrix.GetLength(1); line++, x += FieldConfig.OFFSET_X)
            {
                GUI.Label(new Rect(x, y, FieldConfig.FRUIT_WIDHT, FieldConfig.FRUIT_HEIGHT), fruitsMatrix[collumn, line]);
            }
        }
    }


    /// <summary>
    /// Создаем матрицу фишек без повторений 3 либо более фишек в ряд либо колонкой, либо Т либо Г образной формы.
    /// </summary>
    /// <returns>
    /// Возвращаем двумерный массив фишек.
    /// </returns>
    Texture2D[,] CreateFruitMatrix()
    {
        Texture2D[,] fruitsMatrix = new Texture2D[FieldConfig.Height.height, FieldConfig.Width.width];
        for (int collumn = 0; collumn < fruitsMatrix.GetLength(0); collumn++)
        {
            for (int line = 0; line < fruitsMatrix.GetLength(1); line++)
            {
                fruitsMatrix[collumn, line] = GetFruit();
                if (line > 1)
                {
                    if (fruitsMatrix[collumn, line] == fruitsMatrix[collumn, line - 1] == fruitsMatrix[collumn, line - 2])
                    {
                        fruitsMatrix[collumn, line] = GetFruit();
                        line--;
                    }
                }
                if (collumn > 1)
                {
                    if (fruitsMatrix[collumn, line] == fruitsMatrix[collumn - 1, line] == fruitsMatrix[collumn - 2, line])
                    {
                        fruitsMatrix[collumn, line] = GetFruit();
                        if (line > 0)
                            line--;
                        else
                        {
                            collumn--;
                            line = fruitsMatrix.GetLength(1);
                        }
                    }
                }
            }
        }
        return fruitsMatrix;
    }

    /// <summary>
    /// выбираем случайную фишку для заполнения.
    /// </summary>
    /// <returns>
    /// возвращаем спрайт с фишкой.
    /// </returns>
    Texture2D GetFruit()
    {
        switch (Random.Range(1, 5))
        {
            case 1:
                return apple;
            case 2:
                return grape;
            case 3:
                return pear;
            case 4:
                return mandarine;
            default:
                return null;
        }
    }
}
