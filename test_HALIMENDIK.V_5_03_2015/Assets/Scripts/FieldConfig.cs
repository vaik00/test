﻿using UnityEngine;
using System.Collections;

public class FieldConfig : MonoBehaviour {
	public int width = 8;                     // ширина поля 
	public int height = 8;                    // высота поля

    public const float OFFSET_X = 95f;        // отступ каждой последующей фишки по левому краю
    public const float OFFSET_Y = 75f;        // отступ каждой последующей фишки сверху
    public const float FRUIT_WIDHT = 80f;     // ширина фишки
    public const float FRUIT_HEIGHT = 90f;    // высота фишки
    public const float FRUIT_X = 0f;          // первоначальное расположение фишки по левому краю
    public const float FRUIT_Y = 0f;          // первоначальное расположение фишки сверху
    public const float FIELD_OFFSET_X = 0f;   // отступ фона по левому краю
    public const float FIELD_OFFSET_Y = 0f;   // отступ фона сверху
    public const float FIELD_WIDHT = 1366f;   // ширина поля в пикселях
    public const float FIELD_HEIGHT = 768f;   // высота поля в пикселях
	public static FieldConfig Width { get; private set; }
	public static FieldConfig Height { get; private set; }

	void Awake() {
		CheckFieldSize ();
		Width = this;
		Height = this;
	}

    /// <summary>
    /// Проверка поля, если ширина либо высота поля превышает 8, сбрасываем ширину либо высоту до 8
    /// </summary>
	void CheckFieldSize() {
		if (width > 8)
			width = 8;
		if (height > 8)
			height = 8;
	}
}
